###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import sys

from PySketch.proc      import *
from PySketch.log       import msg, dbg, wrn, err, cri, printPair

###############################################################################
# PROCESS UTILs

def gitClone(repoUrl, dirName=None):
    if dirName is None:
        return launchProcess(["git", "clone", repoUrl])
    
    return launchProcess(["git", "clone", repoUrl, dirName])

def gitPull():
    return launchProcess(["git", "pull"])

def gitAdd(target="."):
    return launchProcess(["git", "add", target])

def gitCommit(msg):
    return launchProcess(["git", "commit", "-m", msg])

def gitPush():
    return launchProcess(["git", "push"])

def make(arg=None):
    if arg is None:
        return launchProcess(["make"])

    return launchProcess(["make", arg])

'''
def skmake(config, cores):
    return launchProcess(["{}/skmake".format(binPath), "-m", config, "-c", "{}".format(cores)])
'''

def aptUpgrade():
    if sys.version_info < (3, 0):
        err("'apt' CANNOT run under Python < 3; ignoring ..")
        return False
        
    return launchProcess(["sudo", "apt", "update"]) and launchProcess(["sudo", "apt", "-y", "upgrade"])

def aptInstall(pcks):
    if sys.version_info < (3, 0):
        err("'apt' CANNOT run under Python < 3; ignoring ..")
        return False

    cli = ["sudo", "apt", "-y", "install"] + pcks
    return launchProcess(cli)

def aptClean():
    if sys.version_info < (3, 0):
        err("'apt' CANNOT run under Python < 3; ignoring ..")
        return False

    cli = ["sudo", "apt", "clean"]
    return launchProcess(cli)

def xbpsUpgrade():
    return launchProcess(["sudo", "xbps-install", "-Syuv"]) and launchProcess(["sudo", "xbps-remove", "-Ooyv"])

def xbpsInstall(pcks):
    cli = ["sudo", "xbps-install", "-y"] + pcks
    return launchProcess(cli)
    
def brewInstall(pcks):
    cli = ["brew", "install"] + pcks
    return launchProcess(cli)

def pkgInstall(pcks):
    cli = ["pkg", "install", "-y"] + pcks
    return launchProcess(cli)

###############################################################################