###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os
import git

from datetime               import datetime

from PySketch.log           import msg, dbg, wrn, err, cri, printPair
from PySketch.term          import *

from PCK.props              import *
from PCK.procs              import *

###############################################################################

class GitRepository():
    def __init__(self, path):
        self._path = os.path.abspath(path)
        self._name = os.path.basename(self._path)
        self._git = git.Repo(self._path)

        dbg("Git Repository path set [{}]: {} - {}".format(pathExists(self._path), self._name, self._path))

    def needToPull(self):
        dbg("Checking PULL status for git-repository: {}".format(self._name))
        self._git.remotes.origin.fetch()

        commitsBehindCount = 0

        if self._git.active_branch.tracking_branch() is not None:
            commitsBehind = self._git.iter_commits('HEAD..' + self._git.active_branch.tracking_branch().name)
            commitsBehindCount = sum(1 for c in commitsBehind)

            if commitsBehindCount > 0:
                wrn("Git-repository need to PULL [{} commits]: {}".format(commitsBehindCount, self._name))
                return True

        return False

    def pull(self):
        dbg("Trying to PULL git-repository: {}".format(self._name))

        if not changeDir(self._path):
            return False

        if not gitPull():
            return False

        msg("Git-repository successfully PULLED")
        return True
        
    def needToPush(self):
        dbg("Checking PUSH status for git-repository: {}".format(self._name))
        self._git.remotes.origin.pull()

        if self._git.is_dirty(untracked_files=True):
            modified = self._git.git.diff(name_only=True).splitlines()

            dbg("Git-repository need to PUSH [{} modified]: {}".format(len(modified), self._name))

            i=1

            for file in modified:
                dbg("[{}] - {}".format(i, file))
                i += 1

            return True

        return False

    def push(self):
        dbg("Trying to PULL git-repository: {}".format(self._name))

        if not changeDir(self._path):
            return False
            
        if not gitAdd():
            return False

        commitMsg = question("Insert commit message")

        if commitMsg is None:
            return False

        if len(commitMsg) == 0:
            wrn("Committing with NON-SENSE message '[..]'")
            commitMsg = "[..]"

        if not gitCommit(commitMsg):
            return False

        if not gitPush():
            return False

        msg("Git-repository successfully PUSHED")
        return True

###############################################################################
