###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os

from PySketch.log           import msg, dbg, wrn, err, cri, printPair
from PySketch.fs            import *

from PCK.git       import GitRepository
from PCK.pack      import Pck

###############################################################################

class RepoSource():
    def __init__(self, baseSystem, name):
        self._base = baseSystem

        if self._base is None:
            cri("BaseSystem instance NOT found!")
            exit(1)

        self._name = name
        self._installPath = "{}.repo".format(os.path.join(self._base._sourcesInstallPath, self._name))
        self._repoPath = os.path.join(self._installPath, self._name)
        self._repoProps = os.path.join(self._repoPath, "pck-src.json")

    def properties(self):
        return readJson(self._repoProps)
        
    def install(self, url):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return False

        msg("Installing Repo: {} ..".format(self._name))

        if self.isInstalled():
            err("Repo is ALREADY installed: {}".format(self._name))
            return False
            
        if not self._base.clone(url, self._installPath, self._name):
            err("Repo installation FAILED: {}".format(self._name))
            return False

        msg("Repo successfully INSTALLED: {}".format(self._name))
        return True
        
    def update(self):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return False

        msg("Updating Repo: {} ..".format(self._name))

        if not self.isInstalled():
            err("Repo NOT found: {}".format(self._name))
            return False

        gitRepo = GitRepository(self._repoPath)

        if gitRepo.needToPull():
            return gitRepo.pull()

        return True

    def remove(self):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return False

        msg("Removing Repo: {} ..".format(self._name))

        if not self.isInstalled():
            err("Repo NOT found: {}".format(self._name))
            return False

        if self._name == self._base._defaultRepoName:
            err("CANNOT remove default Repo: {}".format(self._base._defaultRepoName))
            return False

        ok = removeDirectory(self._installPath)

        if ok:
            msg("Repo successfully REMOVED: {}".format(self._name))

        else:
            err("Repo removal FAILED: {}".format(self._name))

        return ok

    def searchPacks(self, keyword):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return None
            
        packs = listDirectories(self._repoPath)

        pcks = []

        for pck in packs:
            if pck != ".git" and (keyword is None or keyword.lower() in pck.lower()):
                pcks.append(self.getPck(pck))

        return pcks

    def hasPack(self, pckName):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return None

        pckRepoPath = os.path.join(self._repoPath, pckName)
        pckRepoProps = os.path.join(pckRepoPath, "pck.json")
        return (pathExists(pckRepoPath) and pathExists(pckRepoProps))

    def getPck(self, pckName):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return None

        if not self.hasPack(pckName):
            err("Pack NOT found: {}::{}".format(self._name, pckName))
            return None

        return Pck(self._base, self, pckName)

    def getPcks(self, pckNames):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return None

        pcks = []

        for pckName in pckNames:
            pck = self.getPck(pckName)

            if pck is None:
                err("Pack NOT found: {}".format(pckName))
                pcks = None
                break

            pcks.append(pck)

        return pcks

    def isInstalled(self):
        return pathExists(self._installPath)

    def hasPropFile(self, propName):
        return runtimePropertyExist(self._installPath, propName)

###############################################################################