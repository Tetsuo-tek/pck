###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os
import pytz

from PySketch.host          import HostMachine
from PySketch.log           import escapes, raw, msg, dbg, wrn, err, cri, printPair
from PySketch.fs            import *

from PCK.props              import *
from PCK.procs              import *

from PCK.repo               import RepoSource

###############################################################################

baseSystemHolder = None

class BaseSystem():
    def __init__(self):
        global baseSystemHolder
        
        self._root = None
        self._pckHome = None
        self._binPath = None
        self._etcPath = None
        self._shrPath = None
        self._varPath = None
        self._packsInstallPath = None
        self._sourcesInstallPath  = None
        self._fileSystem = None
        self._repoSources = None

        if baseSystemHolder is not None:
            cri("ONLY one BaseSystem instance is ALLOWED!")
            exit(1)

        self._currentUser = os.getenv('USER')
        dbg("Current user: {}".format(self._currentUser))
        
        self._rootEnvVarName    = "PCK_ROOT"

        self._defaultRepoName   = "InoxPacks"
        self._defaultRepoUrl    = "http://gitlab.com/Tetsuo-tek/InoxPacks"

        self._pckTypes          = ["C", "C++", "Sk/C++", "Sk/PySketch", "Python"]
        self._supportedOS       = ["Linux", "FreeBSD", "Darwin"]
        self._supportedLinux    = ["Void", "Debian GNU/Linux", "Ubuntu", "elementary OS"]
        self._aptCompatibles    = ["Ubuntu", "Debian GNU/Linux", "elementary OS"]

        if self._rootEnvVarName in os.environ:
            self._load()

        else:
            dbg("Base-system env NOT found [env: ${}]".format(self._rootEnvVarName))

    def _load(self):
        if self._root is None:
            self._prepare(os.environ.get(self._rootEnvVarName))
            
        if not self.isValid():
            dbg("Base-system NOT found".format(self._rootEnvVarName))
            return False

        self._repoSources = {}

        pckSrcSets = listDirectories(self._sourcesInstallPath)

        for source in pckSrcSets:
            repoName = source.replace(".repo", "")
            self._repoSources[repoName] = RepoSource(self, repoName)
            dbg("Added source repo: {}".format(repoName))

        dbg("Base-system LOADED: {}".format(self._pckHome))
        return True

    def _prepare(self, root):
        self._root                  = os.path.abspath(root)
        self._pckHome               = os.path.join(self._root,      "pck")
        self._binPath               = os.path.join(self._pckHome,   "bin")
        self._etcPath               = os.path.join(self._pckHome,   "etc")
        self._shrPath               = os.path.join(self._pckHome,   "share")
        self._varPath               = os.path.join(self._pckHome,   "var")
        self._packsInstallPath      = os.path.join(self._pckHome,   "var/packs")    # .pck installed-referers with variable-files
        self._sourcesInstallPath    = os.path.join(self._pckHome,   "var/sources")  # .repo installed-referers with variable-files

        self._fileSystem = [
            self._pckHome,
            self._binPath,
            self._etcPath,
            self._shrPath,
            self._varPath,
            self._packsInstallPath,
            self._sourcesInstallPath
        ]

        self._env = \
            "# >>>\n"                           \
            "export PCK_ROOT={}\n"              \
            "export PCK_HOME=$PCK_ROOT/pck\n"   \
            "export PCK_BIN=$PCK_HOME/bin\n"    \
            "export PATH=$PATH:$PCK_BIN\n"      \
            "# <<<".format(self._root)

    def checkPermissions(self, onlyRead=False):
        ok = False

        if onlyRead:
            ok = isReadable(self._root)
        else:
            ok = isWritable(self._root)

        if not ok:
            self._printPrivilegesError(self._root, onlyRead)
            return False

        for directory in self._fileSystem:
            if onlyRead:
                ok = isReadable(directory)
            else:
                ok = isWritable(directory)

            if not ok:
                self._printPrivilegesError(directory, onlyRead)
                return False 
            
        return True

    def _printPrivilegesError(self, path, onlyRead):
        if onlyRead:
            err("Selected path is NOT-READABLE: {}".format(path))
        else:
            err("Selected path is NOT-WRITABLE: {}".format(path))

    def isValid(self):
        if self._pckHome is None:
            return False

        return (os.path.exists(self._pckHome) and os.path.exists(self._binPath)
            and os.path.exists(self._etcPath) and os.path.exists(self._shrPath)
            and os.path.exists(self._varPath) and os.path.exists(self._packsInstallPath)
            and os.path.exists(self._sourcesInstallPath))

    def check(self):
        host = HostMachine()
        host.printInfos()

        if not self.isValid():
            err("Base-system is NOT valid")
            return False

        return True

    def install(self, root):
        if self.isValid():
            err("Base-system is ALREADY installed: {}={}".format(self._rootEnvVarName, os.environ.get(self._rootEnvVarName)))
            return False

        self._prepare(root)

        if self.isValid():
            err("Base-system ALREADY exists: {}".format(root))
            return False

        if not isWritable(self._root):
            err("Selected root-path is NOT-WRITABLE: {}".format(self._root))
            return False

        dbg("Creating pck base-system ..")
        ok = True

        for directory in self._fileSystem:
            if not createDirectory(directory):
                ok = False
                break
        
        if ok:
            defaultRepo = RepoSource(self, self._defaultRepoName)
            ok = defaultRepo.install(self._defaultRepoUrl)

        if ok:
            ok = self._load()

        if ok:
            showShellLines = True

            if self._rootEnvVarName in os.environ:
                showShellLines = False
                configuredRoot = os.environ.get(self._rootEnvVarName).rstrip("/") 

                if configuredRoot != self._root:
                    wrn("PCK_ROOT env-var is set, but it does NOT refer to current root-path [{}], modify with: 'export PCK_ROOT={}'".format(configuredRoot, self._root))
            
            if showShellLines:
                wrn("Add the following lines to your .bashrc (or equivalent) file and re-source it")
                raw("{}{}{}\n".format(escapes["GRY"], self._env, escapes["RESET"]))
            
            msg("Base-system successfully CREATED")

        else:
            msg("Base-system build FAILED")

        return ok

    def update(self):
        if not self.isValid():
            err("Base-system is NOT valid")
            return False

        for _, repo in self._repoSources.items():
            if not repo.update():
                return False

        pcks = self.getInstalledPcks()

        for pck in pcks:
            if not pck.update():
                return False

        return True

    def upgrade(self):
        if not self.isValid():
            err("Base-system is NOT valid")
            return False

        pcks = self.getInstalledPcks()

        for pck in pcks:
            if not pck.upgrade():
                return False

        return True

    def remove(self):
        if not self.isValid():
            err("Base-system is NOT valid")
            return False

        if not self.checkPermissions():
            return False

        return removeDirectory(self._pckHome)

    def clone(self, url, installPath, name):
        if not self.isValid():
            err("Base-system is NOT valid")
            return False

        if not createDirectory(installPath) or not changeDir(installPath) or not gitClone(url, name):
            return False

        ok = (
            writeRuntimeProperty(installPath, "URL", url) and
            writeRuntimeDateTime(installPath, "CLONED", datetime.now(pytz.UTC))
        )

        if not ok:
            removeDirectory(installPath)
        
        return ok

    def searchPacks(self, keyword):
        if not self.isValid():
            err("Base-system is NOT valid")
            return None

        pcks = []

        for _, repo in self._repoSources.items():
            pcks.extend(repo.searchPacks(keyword))

        return pcks

    def getInstalledPcks(self):
        if not self.isValid():
            err("Base-system is NOT valid")
            return None

        pckDirs = listDirectories(self._packsInstallPath)
        pckNames = []

        for pckDir in pckDirs:
            pckNames.append(pckDir.rstrip(".pck"))

        return self.getPcks(pckNames)

    def getPck(self, pckName):
        if not self.isValid():
            err("Base-system is NOT valid")
            return None

        pck = None

        for _, repo in self._repoSources.items():
            if repo.hasPack(pckName):
                pck = repo.getPck(pckName)
                break

        return pck

    def getPcks(self, pckNames):
        if not self.isValid():
            err("Base-system is NOT valid")
            return None

        pcks = []

        for pckName in pckNames:
            pck = self.getPck(pckName)

            if pck is None:
                err("Pack NOT found: {}".format(pckName))
                pcks = None
                break

            pcks.append(pck)

        return pcks

###############################################################################