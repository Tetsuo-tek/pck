import os

from datetime           import datetime

from PySketch.fs        import *
from PySketch.log       import msg, dbg, wrn, err, cri, printPair

###############################################################################
# RUNTIME-FILEPROPs UTILs

def runtimePropertyExist(path, key):
    filePath = os.path.join(path, key)
    return os.path.exists(filePath)

def writeRuntimeProperty(path, key, val):
    filePath = os.path.join(path, key)
    return writeFile(filePath, val, mode="")

def readRuntimeProperty(path, key):
    fileName = os.path.join(path, key)
    return readFile(fileName, mode="")

def writeRuntimeJson(path, key, val):
    return writeRuntimeProperty(path, key, json.dumps(val))
    
def readRuntimeJson(path, key):
    txt = readRuntimeProperty(path, key)

    if txt is None:
        return None
    
    return json.loads(txt)

def writeRuntimeDateTime(path, key, val):
    return writeRuntimeProperty(path, key, val.isoformat())

def readRuntimeDateTime(path, key):
    iso = readRuntimeProperty(path, key)

    if iso is None:
        return None

    return datetime.fromisoformat(iso)

###############################################################################