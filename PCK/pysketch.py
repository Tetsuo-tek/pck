###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os

from PySketch.log           import msg, dbg, wrn, err, cri, printPair
from PySketch.fs            import *

###############################################################################

class PySketchInstaller():
    def __init__(self, baseSystem, pck):
        self._base = baseSystem
        self._pck = pck
        self._sketches = None

    def setup(self, sketches):
        self._sketches = sketches
        #opt future options for sketches
        return True

    def build(self):
        if self._sketches is None:
            err("PySketch is NOT configured")
            return False

        for sketch in self._sketches:
            #make python-env if required
            pass
        
        return True

    def install(self, entries):
        if self._sketches is None:
            err("PySketch is NOT configured")
            return False

        for sketch in self._sketches:
            sketchName = sketch["sketch"]
            sketchFile = os.path.join(self._pck._srcPath, sketchName)

            if pathExists(sketchFile):
                dbg("PySketch code-target CHECKED: {}".format(sketchFile))

            else:
                err("PySketch code-target NOT found: {}".format(sketchFile))
                return False

            os.chmod(sketchFile, 0o755)
            sketchName = os.path.basename(sketchFile)

            destLink = os.path.join(self._base._binPath, sketchName) 

            if not symLink(sketchFile, destLink):
                return False

            entries.append(destLink)

        return True

###############################################################################