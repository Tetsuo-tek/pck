###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os
import pytz

import sys
import importlib.util
import traceback

from PySketch.log           import msg, dbg, wrn, err, cri, printPair
from PySketch.host          import HostMachine
from PySketch.fs            import *
from PySketch.term          import *

from PCK.props              import *
from PCK.procs              import *

from PCK.git                import GitRepository
from PCK.skmake             import SkMakeInstaller
from PCK.pysketch           import PySketchInstaller

###############################################################################

class Pck():
    def __init__(self, baseSystem, repo, name):
        self._base = baseSystem

        if self._base is None:
            cri("BaseSystem instance NOT found!")
            exit(1)

        self._name = name
        self._repo = repo
        self._modName = None
        self._module = None
        
        self._installPath = "{}.pck".format(os.path.join(self._base._packsInstallPath, self._name))
        self._srcPath = os.path.join(self._installPath, self._name)

        self._repoPath          = os.path.join(self._repo._repoPath,    self._name)
        self._repoProps         = os.path.join(self._repoPath,          "pck.json")
        self._repoSysDeps       = os.path.join(self._repoPath,          "sys-deps.json")
        self._repoBuilds        = os.path.join(self._repoPath,          "builds.json")
        self._repoSketches      = os.path.join(self._repoPath,          "sketches.json")
        self._repoLaunchers     = os.path.join(self._repoPath,          "launchers.json")
        self._repoPreInstall    = os.path.join(self._repoPath,          "pre-install.py")
        self._repoPostInstall   = os.path.join(self._repoPath,          "post-install.py")
        self._repoUpgrade       = os.path.join(self._repoPath,          "upgrade.py")
        self._repoRemove        = os.path.join(self._repoPath,          "remove.py")

    def _loadModule(self, codeFile):
        if not pathExists(codeFile) or not codeFile.endswith(".py"):
            err("Python module is NOT valid: {}".format(codeFile))
            return False

        name = os.path.splitext(os.path.basename(codeFile))[0]
        self._modName = "{}_{}_code".format(self._name, name)

        dbg("Loading Python module: {} ..".format(self._modName))

        if self._module is not None:
            err("Python module is ALREADY loaded: {}".format(self._modName))
            return False

        try:
            spec = importlib.util.spec_from_file_location(self._modName, codeFile)
            self._module = importlib.util.module_from_spec(spec)
            sys.modules[self._modName] = self._module
            spec.loader.exec_module(self._module)

        except Exception as e:
            err("Python module LOAD ERROR: {}".format(self._modName))
            err("{}".format(e))
            err("{}".format(traceback.format_exc()))
            self._unloadModule()
            return False

        dbg("Python module LOADED: {}".format(self._modName))
        return True

    def _unloadModule(self):
        if self._module is None or not self._modName in sys.modules:
            err("CANNOT unload Python module that is not loaded")
            return False

        dbg("Unloading Python module: {} ..".format(self._modName))
        del sys.modules[self._modName]
        dbg("Python module UNLOADED: {}".format(self._modName))

        self._module = None
        self._modName = None
        return True

    def properties(self):
        if not pathExists(self._repoProps):
            err("Pack is CORRUPTED, missing 'pck.json': {}".format(self._name))
            return None

        props = readJson(self._repoProps)

        if props is None:
            return None
        
        props["hasPckDeps"] = ("deps" in props)
        props["hasSysDeps"] = pathExists(self._repoSysDeps)
        props["hasBuilds"] = pathExists(self._repoBuilds)
        props["hasPreInstall"] = pathExists(self._repoPreInstall)
        props["hasPostInstall"] = pathExists(self._repoPostInstall)
        props["hasUpgrade"] = pathExists(self._repoRemove)
        props["hasRemove"] = pathExists(self._repoRemove)
        props["isInstalled"] = self.isInstalled()
        
        return props
        
    def install(self):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return False
            
        msg("Installing Pack: {} ..".format(self._name))

        if self.isInstalled():
            err("Pack is ALREADY installed: {}".format(self._name))
            return False
        
        props = self.properties()

        if props is None:
            return False

        currDir = os.getcwd()
        host = HostMachine()

        pck_t = props["type"]

        if not pck_t in self._base._pckTypes:
            err("Pack 'type' UNKNOWN: {}".format(pck_t))
            return False

        hasSysDeps = pathExists(self._repoSysDeps)
        sysDeps = None

        hasDeps = ("deps" in props)
        deps = None

        requiresDirs = ("dirs" in props)
        optDirs = None

        if hasSysDeps:
            sysDeps = self._selectCompatibleSysDepsProfile(host)

            if sysDeps is None or not self._sysDepsInstall(host, sysDeps):
                return False

        if hasDeps:
            deps = props["deps"]

            if not self._depsInstall(deps):
                return False

        if requiresDirs:
            optDirs = props["dirs"]

            if not self._makeDirs(optDirs):
                return False

        if not self._base.clone(props["url"], self._installPath, self._name):
            return False

        if hasSysDeps:
            writeRuntimeJson(self._installPath, "SYSDEPS", sysDeps)

        if hasDeps:
            writeRuntimeJson(self._installPath, "DEPS", deps)

        if requiresDirs:
            writeRuntimeJson(self._installPath, "DIRS", optDirs)

        ok = self._build(pck_t, host)

        changeDir(currDir)

        if ok:
            msg("Pack successfully INSTALLED: {}".format(self._name))

        else:
            err("Pack installation FAILED: {}".format(self._name))

            if self.isInstalled():
                self.remove()

        return ok

    def update(self):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return False

        msg("Updating Pack: {} ..".format(self._name))

        if not self.isInstalled():
            err("Pack NOT installed: {}".format(self._name))
            return False

        gitRepo = GitRepository(self._srcPath)

        if not gitRepo.needToPull():
            dbg("PULL is NOT required: {}".format(self._name))
            return True

        if not gitRepo.pull():
            return False

        writeRuntimeDateTime(self._installPath, "PULLED", datetime.now(pytz.UTC))
        return True

    def upgrade(self):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return False

        msg("Upgrading Pack: {} ..".format(self._name))

        if not self.isInstalled():
            err("Pack NOT installed: {}".format(self._name))
            return False

        ##

        writeRuntimeDateTime(self._installPath, "UPGRADED", datetime.now(pytz.UTC))
        return False

    def remove(self):
        if not self._base.isValid():
            err("Base-system is NOT valid")
            return False

        msg("Removing Pack: {} ..".format(self._name))

        if not self.isInstalled():
            err("Pack NOT installed: {}".format(self._name))
            return False

        if self.hasPropFile("ENTRIES"):
            entries = readRuntimeJson(self._installPath, "ENTRIES")

            if entries is not None:
                for entry in entries:
                    wrn("REMOVING Pack entry item: {}".format(entry))
                    if not removeFile(entry):
                        return False

        if self.hasPropFile("DIRS"):
            dirs = readRuntimeJson(self._installPath, "DIRS")

            for dirPath in dirs:
                wrn("REMOVING Pack optional directory: {}".format(dirPath))
                if not removeDirectory(dirPath):
                    return False

        ###
        if self.hasPropFile("DEPS"):
            pass
        ####

        return removeDirectory(self._installPath)

    def _sysDepsInstall(self, host, deps):
        if len(deps) == 0:
            return True

        wrn("Pck requires system dependencies (sys-deps.json): {}".format(", ".join(deps)))

        if booleanQuestion("Do you want to check for system dependencies"):
            osName = host.infos()["os"]

            if osName == "Linux":
                distro = host.infos()["distro-name"]

                if distro == "Void":
                    return xbpsUpgrade() and xbpsInstall(deps)

                elif distro in self._base._aptCompatibles:
                    return aptUpgrade() and aptInstall(deps) and aptClean()

            elif osName == "FreeBSD":
                return pkgInstall(deps)

            elif osName == "Darwin":
                return brewInstall(deps)

        return True

    def _depsInstall(self, deps):
        if len(deps) == 0:
            return True

        wrn("'{}' has pack-dependencies: {}".format(self._name, ", ".join(deps)))
        pcks = self._base.getPcks(deps)

        if pcks is None:
            err("Pck dependencies NOT found: {}".format(deps))
            return False

        for pck in pcks:
            if pck.isInstalled():
                dbg("'{}' pack-dependency is ALREADY installed: {}".format(self._name, pck._name))
                continue

            if not pck.install():
                return False

        return True

    def _makeDirs(self, dirs):
        if len(dirs) == 0:
            return True

        wrn("Pack requires to create {} optional directories: {}".format(len(dirs), self._name))
        
        for dirName in dirs:
            dirPath = os.path.join(self._base._pckHome, dirName)

            if os.path.exists(dirPath):
                wrn("Pack optional directory ALREADY exists: {}".format(dirPath))

            elif not createDirectory(dirPath):
                return False

        return True

    def _execCustomProgram(self, host, codeFile):
        if not self._loadModule(codeFile):
            return False

        env = {}
        env["baseSystem"] = self._base
        env["pck"] = self
        env["host"] = host
        
        ok = False
        dbg("Launching custom program: {} ..".format(self._modName))

        try:
            ok = self._module.main(env)

        except KeyboardInterrupt:
            err("Forcing exit with CTRL-C")

        except Exception as e:
            err("Custom program execution ERROR: {}".format(self._modName))
            err("{}".format(e))
            err("{}".format(traceback.format_exc()))
        
        self._unloadModule()
        return ok

    def _checkCompatibleProfiles(self, host, profilesFile):
        compatibles = None

        if pathExists(profilesFile):
            dbg("Pack has selectable profiles: {}".format(self._name))
            cfg = readJson(profilesFile)
            
            if cfg is None:
                return None

            compatibles = []
            profiles = cfg["profiles"]

            thisSystem = host.infos()["os"]

            for profile in profiles:
                if thisSystem == profile["os"]:
                    if thisSystem == "Linux":
                        thisDistro = host.infos()["distro-name"]
                        thisVersion = host.infos()["distro-version"]
                        distro = profile["distro"]

                        if distro == thisDistro:
                            if "versions" in profile:
                                versions = profile["versions"]

                                if thisVersion in versions:
                                    compatibles.append(profile)

                            else:
                                compatibles.append(profile)
                            
                    else:
                        compatibles.append(profile)

            if len(compatibles) == 0:
                wrn("Pack has NOT profiles compatible with backend operating system [{}]: {}".format(thisSystem, self._name))
                return None

        else:
            wrn("Pack has NOT selectable profiles: {}".format(self._name))

        return compatibles

    def _selectCompatibleProfile(self, host, profileFile):
        compatibles = self._checkCompatibleProfiles(host, profileFile)

        if compatibles is None:
            return None

        wrn("There are {} profiles".format(len(compatibles)))
        names = []

        for profile in compatibles:
            names.append(profile["name"])

        index = requestForChoice(names)

        if index == -1:
            return None

        return compatibles[index]

    def _selectCompatibleBuildProfile(self, host, skmake):
        if skmake.hasDefaultConfig():
            return skmake.getDefaultConfigName()

        profile = self._selectCompatibleProfile(host, self._repoBuilds)

        if profile is None:
            return None

        profileDataList = profile["configs"]
        wrn("Profile for this system contains {} build-configurations".format(len(profileDataList)))

        index = requestForChoice(profileDataList)

        if index == -1:
            return None

        return profileDataList[index]

    def _selectCompatibleSysDepsProfile(self, host):
        profile = self._selectCompatibleProfile(host, self._repoSysDeps)

        if profile is None:
            return None

        profileDataList = profile["packages"]
        
        if len(profileDataList) > 0:
            wrn("Profile for this system contains {} sys-deps".format(len(profileDataList)))

        return profileDataList

    def _buildSkMakePrj(self, host):
        skmake = SkMakeInstaller(self._base, self)
        cfgFile = self._selectCompatibleBuildProfile(host, skmake)

        if cfgFile is None:
            wrn("Pack successfully INSTALLED (without build): {}".format(self._name))

        return (skmake.setup(cfgFile)
                and skmake.build(host.infos()["cpu-cores"])
                and skmake.install()
                and writeRuntimeJson(self._installPath, "SKMAKECFG", skmake._currentCfg)
                and writeRuntimeJson(self._installPath, "ENTRIES", ["{}".format(skmake._destLink)]))

    def _buildPySketchPrj(self, host):
        if not pathExists(self._repoSketches):
            return False
        
        pysketch = PySketchInstaller(self._base, self)
        sketches = readJson(self._repoSketches)

        if sketches is None:
            err("Pack has NOT valid sketches configuration: {}".format(self._name))
            return False
        
        entries = []
        return (pysketch.setup(sketches)
                and pysketch.build()
                and pysketch.install(entries)
                and writeRuntimeJson(self._installPath, "ENTRIES", entries))

    def _buildPythonPrj(self, host):
        return True

    def _build(self, pck_t, host):
        ok = True
        
        if pathExists(self._repoPreInstall):
            ok = self._execCustomProgram(host, self._repoPreInstall)

        if ok:
            if pck_t == "Sk/C++":
                ok = self._buildSkMakePrj(host)

            elif pck_t == "Sk/PySketch":
                ok = self._buildPySketchPrj(host)

            elif pck_t == "Python":
                ok = self._buildPythonPrj(host)
            
            else:
                wrn("Building-way NOT selected")
                
        if ok and pathExists(self._repoPostInstall):
            ok = self._execCustomProgram(host, self._repoPostInstall)

        return (ok and writeRuntimeDateTime(self._installPath, "INSTALLED", datetime.now(pytz.UTC)))

    def isInstalled(self):
        return pathExists(self._installPath)

    def hasPropFile(self, propName):
        return runtimePropertyExist(self._installPath, propName)

###############################################################################