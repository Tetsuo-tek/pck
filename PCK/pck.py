#!/usr/bin/env python

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import sys
import os

import argparse

from PySketch.log           import LogMachine, escapes, printPair
from PySketch.fs            import *
from PySketch.term          import *

from PCK.git                import GitRepository
from PCK.base               import BaseSystem

###############################################################################
###############################################################################
# GLOBAL

codeName                = "The Riot" 
version                 = [0,0,0]
author                  = "Daniele Di Ottavio (aka Tetsuo)"
copyleft                = "2024"

baseSystem              = None
currentUser             = None
log                     = None

###############################################################################

def main():
    global baseSystem
    global log
    global currentUser

    currDir = os.getcwd()

    parser = argparse.ArgumentParser(description="PCK meta-package manager")
    parser.add_argument("--version",        action="store_true",    help="Show pck version")
    
    parser.add_argument("--check-base",     action="store_true",    help="Check pck-fs consistency")
    parser.add_argument("--install-base",                           help="Install base-system on the given root-path")
    parser.add_argument("--remove-base",    action="store_true",    help="Remove base-system")

    parser.add_argument("--list",           action="store_true",    help="Print installed packs")

    parser.add_argument("--search",                                 help="Search for packs by name")

    parser.add_argument("--info",                                   help="Print pack informations")
    parser.add_argument("--install",        nargs="*",              help="Install and build pack")
    parser.add_argument("--remove",         nargs="*",              help="Remove pack")
    parser.add_argument("--update",         nargs="*",              help="Check for changes")
    parser.add_argument("--upgrade",        nargs="*",              help="Evaluate changes and rebuild")

    parser.add_argument("--push",                                   help="Add committing files, commit and push some git-repository")

    parser.add_argument("--fix",            action="store_true",    help="Try to fix pck-fs unconsistencies")

    args = parser.parse_args()

    log = LogMachine()
    log.setNodeName(os.path.basename(sys.argv[0]))
    baseSystem = BaseSystem()

    ok = False

    ###############################################################################

    if args.version:
        log.msg("Version: {} ({})".format(".".join(str(num) for num in version), codeName))
        ok = True

    ###############################################################################

    elif args.fix:
        log.msg("Trying to fix pck file-system ..")
        #ok = baseSystem.fix()
        pass

    elif args.check_base:
        log.msg("Checking pck base-system ..")
        ok = baseSystem.check()

    elif args.install_base:
        root = args.install_base
        log.msg("Installing pck base-system on root-path: {} ..".format(root))
        ok = baseSystem.install(root)

    elif args.remove_base:
        log.msg("Removing pck base-system ..")
        ok = baseSystem.remove()

    ###############################################################################

    elif args.list:
        pcks = baseSystem.getInstalledPcks()
        
        if pcks is not None:
            ok = True

            if len(pcks) > 0:
                log.raw("{}{} installed packs checked:\n".format(escapes["YLW"], len(pcks)))
                
                if pcks is not None:
                    for i, pck in enumerate(pcks, start=1):
                        log.raw("{}{}{} [repo: {}] {}-> {}{}\n".format(
                            escapes["DYLW"],
                            i,
                            escapes["GRY"],
                            pck._repo._name,
                            escapes["WHT"],
                            escapes["CYN"],
                            pck._name)
                        )

                    log.raw(escapes["RESET"])

            else:
                log.wrn("No Packs INSTALLED")

    elif args.search:
        keyword = args.search
        log.msg("Searching packs [keyword: '{}'] ..".format(keyword))

        pcks = baseSystem.searchPacks(keyword)
        
        if pcks is not None:
            ok = True

            if len(pcks) > 0:
                log.raw("{}{} packs found:\n".format(escapes["YLW"], len(pcks)))
                for i, pck in enumerate(pcks, start=1):
                    log.raw("{}{}{} [repo: {}; installed: {}] {}-> {}{}\n".format(
                        escapes["YLW"],
                        i, escapes["GRY"],
                        pck._repo._name,
                        pck.isInstalled(),
                        escapes["WHT"],
                        escapes["CYN"], pck._name)
                    )

                log.raw(escapes["RESET"])

            else:
                log.wrn("No Packs FOUND")
            
    elif args.info:
        pckName = args.info
        log.msg("Reporting pack info: {}".format(pckName))
        pck = baseSystem.getPck(pckName)

        if pck is not None:
            props = pck.properties()

            for k, v in props.items():
                printPair(k, v)

            ok = True

        else:
            log.err("Pack NOT found: {}".format(pckName))

    ###############################################################################

    elif args.install:
        pckNames = args.install
        pcks = baseSystem.getPcks(pckNames)

        if pcks is not None:
            ok = True

            for pck in pcks:
                if pck.isInstalled():
                    log.wrn("Pack is ALREADY installed: {}".format(pck._name))
                    continue

                if not pck.install():
                    ok = False
                    break

    elif args.update is not None:
        pckNames = args.update

        if pckNames:
            pcks = baseSystem.getPcks(pckNames)

            if pcks is not None:
                ok = True

                for pck in pcks:
                    if not pck.isInstalled():
                        log.wrn("Pack is NOT installed: {}".format(pck._name))
                        continue

                    if not pck.update():
                        ok = False
                        break
        else:
            ok = baseSystem.update()

    elif args.upgrade is not None:
        pckNames = args.upgrade

        if pckNames:
            pcks = baseSystem.getPcks(pckNames)

            if pcks is not None:
                ok = True

                for pck in pcks:
                    if not pck.isInstalled():
                        log.wrn("Pack is NOT installed: {}".format(pck._name))
                        continue

                    if not pck.upgrade():
                        ok = False
                        break
        else:
            ok = baseSystem.upgrade()

    elif args.remove:
        pckNames = args.remove
        pcks = baseSystem.getPcks(pckNames)

        if len(pcks) == 0:
            log.err("Pack names to remove are REQUIRED")

        elif pcks is None:
            log.err("Pack NOT found")

        else:
            ok = True

            for pck in pcks:
                if not pck.isInstalled():
                    log.wrn("Pack is NOT installed: {}".format(pck._name))
                    continue

                if not pck.remove():
                    ok = False
                    break

    ###############################################################################

    elif args.push:
        path = args.push

        if pathExists(path):
            gitRepo = GitRepository(path)

            if gitRepo.needToPush():
                ok = gitRepo.push()
            
            else:
                log.msg("Git-repository does NOT need to PUSH: {}".format(path))
                ok = True
        
        else:
            log.err("Git-repository NOT found: {}".format(path))
            ok = False

    ###############################################################################

    else:
        log.wrn("Nothing to do!")
        changeDir(currDir)
        exit(0)

    changeDir(currDir)

    if not ok:
        log.cri("Exiting with ERRORs!")
        exit(1)

    log.msg("End.")

    ###############################################################################

if __name__ == "__main__":
    main()

###############################################################################
###############################################################################