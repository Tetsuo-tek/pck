###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os

from PySketch.log           import msg, dbg, wrn, err, cri, printPair
from PySketch.fs            import *

from PCK.procs     import *

###############################################################################

class SkMakeInstaller():
    def __init__(self, baseSystem, pck):
        self._base = baseSystem
        self._pck = pck
        self._makeFile = os.path.join(self._pck._srcPath, "Makefile")
        self._defaultCfgFile = os.path.join(self._pck._srcPath, "skmake.json")
        self._currentCfgFile = None
        self._currentCfg = None
        self._binFile = None
        self._destLink = None

    def setup(self, cfgFile):
        specialkSrcPath = "{}.pck/SpecialK".format(os.path.join(self._base._packsInstallPath, "SpecialK"))
        destLink = "{}".format(os.path.join(self._pck._installPath, "SpecialK"))

        if not symLink(specialkSrcPath, destLink):
            return False

        if not changeDir(self._pck._srcPath):
            return False
            
        self._currentCfgFile = cfgFile
        self._currentCfg = readJson(self._currentCfgFile)

        if self._currentCfg is None:
            return False

        self._binFile = os.path.abspath(self._currentCfg["target"])

        dbg("SkMake config selected [{}]: {}".format(self._pck._name, self._currentCfgFile))
        return True

    def build(self, cores):
        if self._binFile is None:
            err("SkMake is NOT configured")
            return False
            
        dbg("Building Pack with SkMake: {}".format(self._pck._name))

        if pathExists(self._makeFile):
            if not make("distclean"):
                return False

        if not launchProcess(["{}/skmake".format(self._base._binPath), "-m", self._currentCfgFile, "-c", "{}".format(cores)]):
            return False
        
        if pathExists(self._makeFile):
            if not make("clean"):
                return False

        dbg("Pack successfully BUILD through SkMake: {}".format(self._pck._name))
        return True

    def install(self):
        if self._binFile is None:
            err("SkMake is NOT configured")
            return False

        if pathExists(self._binFile):
            dbg("SkMake binary-target CHECKED: {}". format(self._binFile))

        else:
            err("SkMake binary-target NOT found: {}".format(self._binFile))
            return False
        
        os.chmod(self._binFile, 0o755)
        renamed = self._pck._name.lower()
        self._destLink = os.path.join(self._base._binPath, renamed) 
        return symLink(self._binFile, self._destLink)

    def hasDefaultConfig(self):
        return pathExists(self._defaultCfgFile)

    def getDefaultConfigName(self):
        return os.path.basename(self._defaultCfgFile)

###############################################################################