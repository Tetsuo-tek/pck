###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

import os
import sys
from setuptools import setup, find_packages

package_dir = '.'

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

if sys.version_info < (3, 0):
    with open("requirements_py2.txt") as f:
        requirements = f.read().splitlines()
else:
    with open("requirements_py3.txt") as f:
        requirements = f.read().splitlines()

setup(
    name="PCK",
    version="1.0",
    packages=find_packages(where=package_dir),
    package_dir={"": package_dir},
    include_package_data=True,
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=requirements,
    entry_points={
        'console_scripts': [
            'pck=PCK.pck:main'
        ],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPLv2 License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.0',
    use_scm_version=False,
    license='GPLv2',
    license_files=('LICENSE',),
)
