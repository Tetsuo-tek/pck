# PCK

Pck is a repository manager and more generally a meta-package manager. It was developed to manage the numerous applications built around [SpecialK](https://gitlab.com/Tetsuo-tek/SpecialK), [SkRobot](https://gitlab.com/Tetsuo-tek/SkRobot), and [PySketch](https://gitlab.com/Tetsuo-tek/PySketch).

This is a very young software developed from scratch for specific contingent needs, but it is already capable of performing the required operations, such as setting up its environment and searching, installing, and removing applications available in the configured Repositories.

Currently, the only configured repository is [InoxPacks](https://gitlab.com/Tetsuo-tek/InoxPacks), which is the default for the environment. However, as explained below, it is possible to add new ones with different software sources.

Pck is entirely written in Python and supports both versions 2.x and 3.x.

## Installation

The systems supported by pck are: void-linux, ubuntu, debian, homebrew (MacOS). Since pck also manages Sk/C++ packages or more generally C and C++ packages, it is necessary to install the C++ development tools environment (`gcc`, `make` and `cmake` if you want) to build programs.

You need to clone the project and install its requirements via pip (assuming you are using Python-3.x and that pip is installed).

To install pck globally, you need to acquire root privileges.

```sh
pip install git+https://gitlab.com/Tetsuo-tek/PySketch
pip install git+https://gitlab.com/Tetsuo-tek/pck
```

At this point, you can install and initialize pck-filesystem (here on home directory, `~` or `$HOME`).

```sh
pck --install-base ~
```

As described from the last message of the base installation process, it is required to set environment variables:

```sh
export PCK_ROOT=<your_pck_root>
export PCK_HOME=$PCK_ROOT/pck
export PCK_BIN=$PCK_HOME/bin
export PATH=$PATH:$PCK_BIN
```

Now, you can use `source <your_env_file>` to reload environment, or simply reopen the terminal.

## Common Operations

You can check the status of the pck ecosystem at any time as follows:

```sh
pck --check-base
```

Search for a pack (case-unsensitive):

```sh
pck --search <only_text_without_wildcards>
```

Remember that pack names are case-sensitive.

Print the list of installed packs:

```sh
pck --list
```

Print information for a pack:

```sh
pck --info <exact_pack_name>
```

Install a pack:

```sh
pck --install <exact_pack_name1> [<exact_pack_name2> <exact_pack_name3> ..]
```

Pack installation may include automatic binary compilation phases if any. When necessary, during the installation phase, the program can request which build process to perform among those prepared (e.g., whether to build with CUDA or not).

To create the Makefile and compile Sk/C++ applications (based on [SpecialK](https://gitlab.com/Tetsuo-tek/SpecialK)), the [SkMake](https://gitlab.com/Tetsuo-tek/SkMake) tool is necessary.

Remove a pack (pack name is case-sensitive):

```sh
pck --remove <exact_pack_name1> [<exact_pack_name2> <exact_pack_name3> ..]
```

Update, or Upgrade, PackRepos or a defined pack set (pack name is case-sensitive):

```sh
pck --update [<exact_pack_name1> <exact_pack_name2> <exact_pack_name3> ..]
pck --upgrade [<exact_pack_name1> <exact_pack_name2> <exact_pack_name3> ..]
```


You can view all available commands by asking for help from pck:

```sh
pck -h
```

## Removing the pck Ecosystem and Filesystem

To remove any trace of pck, simply run the following command:

```sh
pck --remove-base
```

## Creating a New Repository

An additional repository for pck consists of a regular git-compatible repository, for example, on GitLab or even GitHub, or on local servers also based on git.

The project folder must contain a pck-src.json file with the Repo name; more properties will be added in the future. The file looks like this (it relates to the default Repo [InoxPacks](https://gitlab.com/Tetsuo-tek/InoxPacks)):

```json
{
    "name": "InoxPacks"
}
```

Each pack managed by the Repo must be associated with a directory named as the same. Inside the directory associated with each pack, there must be always a pck.json file to identify the type of meta-package. The currently supported package types are:

* Sk/C++
* Sk/PySketch
* Python

Below are two examples, one for a Sk/C++ application and one for a Sk/PySketch:

A Sk/C++ application:

```json
{
    "name": "SkRobot",
    "type": "Sk/C++",
    "dirs": ["var/robot"],
    "url": "https://gitlab.com/Tetsuo-tek/SkRobot",
    "src": "InoxPacks",
    "repo": "GIT"
}
```

A PySketch:

```json
{
    "name": "robot-nao-io",
    "type": "Sk/PySketch",
    "url": "https://gitlab.com/Tetsuo-tek/robot-nao-io",
    "src": "InoxPacks",
    "repo": "GIT"
}
```

Depending on the type of pack, you can include other files to specify and customize the pack's installation.

All types of applications can include a program file:
* install.py - a regular Python program that is run as a customization during the installation process.

The directory related to a Sk/C++ application can also contain:
* builds.json - which refers to different modes to compile the application, e.g. considering or not considering CUDA support.

A build.json file refers to different systems, and for each system, it can be associated with multiple skmake.json files, with which the [SkMake](https://gitlab.com/Tetsuo-tek/SkMake) utility can generate the Makefile and then compile to produce the binary for the application. Below is an example of builds.json:

```json
{
    "profiles" : [
        {
            "name" : "ubuntu",
            "os" : "Linux",
            "distro" : "Ubuntu",
            "configs" : ["skmake-ubuntu-nocuda.json", "skmake-ubuntu-cuda.json"]
        },
        {
            "name" : "debian",
            "os" : "Linux",
            "distro" : "Debian GNU/Linux",
            "configs" : ["skmake-ubuntu-nocuda.json"]
        },
        {
            "name" : "void",
            "os" : "Linux",
            "distro" : "Void",
            "configs" : ["skmake-void.json"]
        },
        {
            "name" : "mac-brew",
            "os" : "Darwin",
            "configs" : ["skmake-brew.json"]
        }
    ]
}
```

A Sk/PySketch application must contain the sketches.json file, necessary to build the entries in bin/ related to the sketches. Such a file looks as follows:

```json
[
    {
        "sketch" : "nao-publisher-template.py"
    },
    {
        "sketch" : "nao-subscriber-template.py"
    }
]
```

As an example and to keep track of future developments, refer to the default Repo [InoxPacks](https://gitlab.com/Tetsuo-tek/InoxPacks).